<!DOCTYPE html>
<html>

	<head>
		<title>Home - Moulin St. Blaise</title> <!-- Titre de la page (Doit être différente pour chaque page) -->
		<meta name="description" content="Situé à Chahaignes (Sarthe), le Moulin St. Blaise vous accueille dans le Sud-Sarthe" /> <!-- Description de la page pour les moteurs de recherche (Doit être différente pour chaque page) -->
		<?php include("stuff.php"); ?>
	</head>

	<body>
		<?php include("menu.php"); ?>

		<div class="container center">
			<img class="logo" src="/stblaise/stuff/img/logo.png">
			<img class="logo-m" src="/stblaise/stuff/img/logo-m.png">
		</div>

		<div class="container">
			<p>Le Moulin de St Blaise, ancien moulin à eau situé dans la magnifique vallée du Loir, propose deux types d’accueil (chambre d’hôtes et gîte) et ce tout au long de l’année pour des groupes de 2 à 14 personnes. Quel que soit votre âge, nous sommes convaincus qu’il y en a chez nous pour tous les goûts !</p>
		</div>

		<div class="container">
			<div class="accordion">
  			<ul>
			    <li>
    			    <div>
       				<a href="#">
       					<p>"A stunning, lovely location. Thanks !"</p>
                 <h2>&#8210; Dave England</h2>
      				</a>
    				</div>
    			</li>
    			<li>
    				<div>
    			    <a href="#">
    			    	<p>"A perfect slice of calm and tranquility. Many thanks."</p>
                 <h2>&#8210; Graeme and Sam Cappi, Matthew (6) and Megan (3)</h2>
      				</a>
      				</div>
    			</li>
    			<li>
    				<div>
       				<a href="#">
       					<p>"Wow – fantastic – wonderful. Any more words to say ‘Excellent’. See you again VERY soon!"</p>
                 <h2>&#8210; Mike & Kathryn Norman, Louis (6) and Rudi (6 months)</h2>
       				</a>
      				</div>
    			</li>
    			<li>
    				<div>
       				<a href="#">
       					<p>"What a great place you have – lots of luck with your plans for the future"</p>
                 <h2>&#8210; Becky Nevell</h2>
       				</a>
      				</div>
    			</li>
    			<li>
    				<div>
       				<a href="#">
       					<p>"Peaceful, relaxing, good fun trying the wines and the river’s not too cold!"</p>
                 <h2>&#8210; Barney Dunton</h2>
       				</a>
      				</div>
    			</li>
    			<li>
      				<div>
       				<a href="#">
       					<p>"We came for the Le Mans 24 hrs. The week turned into a holiday. What a wonderful way to go."</p>
                 <h2>&#8210; Andy, Ash, Steve, Neil and Toney</h2>
       				</a>
       				</div>
    			</li>
  			</ul>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<p class="nmt">À mi-chemin entre Le Mans et Tours, notre moulin est idéalement situé si vous souhaitez visiter les majestueux châteaux de la Loire, les vignes (sans oublier de déguster les vins locaux, bien sûr), ainsi que le célèbre circuit de course automobile du Mans. Il est également possible de pratiquer différentes activités dans les environs telles que les sports nautiques, le cyclisme, la pêche ou bien le golf.</p>
					<p class="mgt">Vous pouvez aussi choisir de vous détendre en trempant les pieds dans l’eau ou en vous promenant sur notre terrain de 4 hectares. Un cours d’eau passe sous le moulin et la vieille roue à aubes est toujours en place et en attente d’être restaurée. La plupart des chambres donnent sur le cours d’eau qui débouche de sous le moulin et ont vue sur des champs s’étirant à perte de vue.</p>
				</div>
				<div class="col-md-3 center">
					<fieldset id="f1">
        		<div id="map"></div>
      		</fieldset>
				</div>
			</div>
		</div>

		<div class="container">
			<p>Le moulin comprend 4 chambres ainsi qu’une pièce commune et peut accueillir jusqu’à 10 personnes. Plusieurs éléments d’époque tels que le four à pain, l’ancienne pompe à eau et les mécanismes du vieux moulin ont été soigneusement conservés et restaurés. Le gîte, la Maison du Frère, peut accueillir jusqu’à 4 personnes et se situe à l’autre extrémité du bâtiment près du verger où poussent cerisiers et noyers.</p>
		</div>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEfBVhuwCYiNSdyX6T_sjrQxrt6Hw8y2I&callback=initMap"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script type="text/javascript" src="/stblaise/stuff/js/perso.js"></script>
	</body>

	<?php include 'footer.php'; ?>