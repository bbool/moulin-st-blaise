<div class="container-menu">
  <div class="xs-menu-cont">
    <a id="menutoggle">
      <i class="fa fa-align-justify"></i>
    </a>
    <nav class="xs-menu displaynone">
      <ul>
        <li class="active">
          <a href="/stblaise/fr/index.php">
            <i class="fa fa-fw fa-home"></i> ACCUEIL
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-bed" aria-hidden="true"></i> CHAMBRES BED & BREAKFAST
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-users" aria-hidden="true"></i> GÎTE DU FRÈRE
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-fw fa-map-o" aria-hidden="true"></i> ENVIRONS
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-fw fa-calendar" aria-hidden="true"></i> RESERVATIONS
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-fw fa-car" aria-hidden="true"></i> COMMENT VENIR
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-fw fa-phone" aria-hidden="true"></i>CONTACT
          </a>
        </li>
      </ul>
    </nav>
  </div>

  <nav class="menu">
    <ul>
      <li class="active">
        <a href="/stblaise/fr/index.php">
          <i class="fa fa-fw fa-home"></i>ACCUEIL
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-bed" aria-hidden="true"></i> CHAMBRES BED & BREAKFAST
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-users" aria-hidden="true"></i> GÎTE DU FRÈRE
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-fw fa-map-o" aria-hidden="true"></i> ENVIRONS
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-fw fa-calendar" aria-hidden="true"></i>RESERVATIONS
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-fw fa-car" aria-hidden="true"></i> COMMENT VENIR
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-fw fa-phone" aria-hidden="true"></i>CONTACT
        </a>
      </li>
      <li class="has-sub">
        <a href="#">
          <i class="fa fa-flag-o" aria-hidden="true"></i> LANGUES <i class="fa fa-angle-down" aria-hidden="true"></i>
        </a>
        <ul>
          <li>
            <a href="/stblaise/en/index.php">
              <span>
                <img src="/stblaise/stuff/css/images/gb.png"/> ENGLISH
              </span>
            </a>
          </li>
          <li>
            <a href="/stblaise/fr/index.php">
              <span>
                <img src="/stblaise/stuff/css/images/fr.png"/> FRANÇAIS
              </span>
            </a>
          </li>
        </ul>
      </li>
      <li class="info">
        <a href="http://www.mywebtricks.com/">
          <i class="fa fa-info-circle" aria-hidden="true"></i>
        </a>
      </li>
    </ul>
  </nav>
</div>