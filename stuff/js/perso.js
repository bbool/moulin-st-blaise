$(document).ready(function() {
	//responsive menu toggle
	$("#menutoggle").click(function() {
		$('.xs-menu').toggleClass('displaynone');
	});
	//add active class on menu
	$('ul li').click(function(e) {
		e.preventDefault();
		$('li').removeClass('active');
		$(this).addClass('active');
	});
	//drop down menu	
	$(".drop-down").hover(function() {
		$('.mega-menu').addClass('display-on');
	});
	$(".drop-down").mouseleave(function() {
		$('.mega-menu').removeClass('display-on');
	});
});

function initMap() {
  var gite = {
  	lat: 47.744643, 
  	lng: 0.543607,
  	title: 'Le Moulin de St. Blaise'
  };

  var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 7,
  center: gite,
  zoomControl: true,
  streetViewControl: false,
  mapTypeControl: false,
  fullscreenControl: false
  });

  var marker = new google.maps.Marker({
  position: gite,
  map: map
  });

  var map2 = new google.maps.Map(document.getElementById('map2'), {
  zoom: 14,
  center: gite
  });
}