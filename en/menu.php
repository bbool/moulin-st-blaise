<div class="container-menu">
  <div class="xs-menu-cont">
    <a id="menutoggle">
      <i class="fa fa-align-justify"></i>
    </a>
    <nav class="xs-menu displaynone">
      <ul>
        <li class="active">
          <a href="#">
            <i class="fa fa-fw fa-home"></i> HOME
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-bed" aria-hidden="true"></i> BED & BREAKFAST ROOMS
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-users" aria-hidden="true"></i> SELF-GATERING GITE
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-fw fa-map-o" aria-hidden="true"></i> LOCAL AREA
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-fw fa-calendar" aria-hidden="true"></i> GROUP BOOKINGS
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-fw fa-car" aria-hidden="true"></i> LOCATION
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-fw fa-phone" aria-hidden="true"></i>CONTACT
          </a>
        </li>
      </ul>
    </nav>
  </div>

  <nav class="menu">
    <ul>
      <li class="active">
        <a href="#">
          <i class="fa fa-fw fa-home"></i>HOME
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-bed" aria-hidden="true"></i> BED & BREAKFAST ROOMS
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-users" aria-hidden="true"></i> SELF-GATERING GITE
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-fw fa-map-o" aria-hidden="true"></i> LOCAL AREA
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-fw fa-calendar" aria-hidden="true"></i>GROUP BOOKINGS
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-fw fa-car" aria-hidden="true"></i> LOCATION
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-fw fa-phone" aria-hidden="true"></i>CONTACT
        </a>
      </li>
      <li class="has-sub">
        <a href="#">
          <i class="fa fa-flag-o" aria-hidden="true"></i> LANGUAGES <i class="fa fa-angle-down" aria-hidden="true"></i>
        </a>
        <ul>
          <li>
            <a href="/stblaise/en/index.php">
              <span>
                <img src="/stblaise/stuff/css/images/gb.png"/> ENGLISH
              </span>
            </a>
          </li>
          <li>
            <a href="/stblaise/fr/index-fr.php">
              <span>
                <img src="/stblaise/stuff/css/images/fr.png"/> FRANÇAIS
              </span>
            </a>
          </li>
        </ul>
      </li>
      <li class="info">
        <a href="http://www.mywebtricks.com/">
          <i class="fa fa-info-circle" aria-hidden="true"></i>
        </a>
      </li>
    </ul>
  </nav>
</div>