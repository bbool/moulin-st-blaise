<!DOCTYPE html>
<html>

	<head>
		<title>Home - Moulin St. Blaise</title> <!-- Titre de la page (Doit être différente pour chaque page) -->
		<meta name="description" content="Situé à Chahaignes (Sarthe), le Moulin St. Blaise vous accueille dans le Sud-Sarthe" /> <!-- Description de la page pour les moteurs de recherche (Doit être différente pour chaque page) -->
		<?php include("stuff.php"); ?>
	</head>

	<body>
		<?php include("menu.php"); ?>

		<div class="container center">
			<img class="logo" src="/stblaise/stuff/img/logo.png">
			<img class="logo-m" src="/stblaise/stuff/img/logo-m.png">
		</div>

		<div class="container">
			<p>A converted water mill in the beautiful Loir Valley, Le Moulin St Blaise offers both bed and breakfast and self-catering accommodation for parties of from 2 to 14 people throughout the year. Whatever your age, we believe that there is something here for everyone.</p>
		</div>

		<div class="container">
			<div class="accordion">
  			<ul>
			    <li>
    			    <div>
       				<a href="#">
       					<p>"A stunning, lovely location. Thanks !"</p>
                 <h2>&#8210; Dave England</h2>
      				</a>
    				</div>
    			</li>
    			<li>
    				<div>
    			    <a href="#">
    			    	<p>"A perfect slice of calm and tranquility. Many thanks."</p>
                 <h2>&#8210; Graeme and Sam Cappi, Matthew (6) and Megan (3)</h2>
      				</a>
      				</div>
    			</li>
    			<li>
    				<div>
       				<a href="#">
       					<p>"Wow – fantastic – wonderful. Any more words to say ‘Excellent’. See you again VERY soon!"</p>
                 <h2>&#8210; Mike & Kathryn Norman, Louis (6) and Rudi (6 months)</h2>
       				</a>
      				</div>
    			</li>
    			<li>
    				<div>
       				<a href="#">
       					<p>"What a great place you have – lots of luck with your plans for the future"</p>
                 <h2>&#8210; Becky Nevell</h2>
       				</a>
      				</div>
    			</li>
    			<li>
    				<div>
       				<a href="#">
       					<p>"Peaceful, relaxing, good fun trying the wines and the river’s not too cold!"</p>
                 <h2>&#8210; Barney Dunton</h2>
       				</a>
      				</div>
    			</li>
    			<li>
      				<div>
       				<a href="#">
       					<p>"We came for the Le Mans 24 hrs. The week turned into a holiday. What a wonderful way to go."</p>
                 <h2>&#8210; Andy, Ash, Steve, Neil and Toney</h2>
       				</a>
       				</div>
    			</li>
  			</ul>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<p class="nmt">Situated mid-way between Tours and Le Mans we are ideally located for sight-seeing tours to the awe-inspiring Chateaux of the Loire, visiting local vineyards (and tasting the produce of course), water sports, cycling, fishing, golf, or visiting the world famous motor racing circuit at Le Mans.</p>
					<p class="mgt">Alternatively you could just kick back and relax, dangle your feet in the stream, or take a gentle stroll around our 10 acre grounds. The mill stream runs through the centre of the Mill House and the old water wheel is still in place (a restoration project just waiting to be done). Most of the rooms overlook the beautiful mill pond with views stretching out onto open fields.</p>
				</div>
				<div class="col-md-3 center">
					<fieldset id="f1">
        		<div id="map"></div>
      		</fieldset>
				</div>
			</div>
		</div>

		<div class="container">
			<p>The Mill House has 4 bed and breakfast rooms (including a family room) and sleeps up to 10 people and is full of original features including a bread oven, old water pump and the workings of the old mill which have been lovingly restored. The gite, La Maison du Frere, sleeps 2-4 people and is situated at the opposite end of the mill buildings near the orchard with cherry and walnut trees.</p>
		</div>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEfBVhuwCYiNSdyX6T_sjrQxrt6Hw8y2I&callback=initMap"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script type="text/javascript" src="/stblaise/stuff/js/perso.js"></script>
	</body>

	<?php include 'footer.php'; ?>