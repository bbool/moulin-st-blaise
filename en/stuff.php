<meta charset="utf-8" /> <!-- Permet l'affichage des caractères spéciaux (utf-8) -->
<meta name="viewport" content="width=device-width, initial-scale=1" /> <!-- Permet l'affichage du site sur mobile -->
<link rel="stylesheet" href="/stblaise/stuff/css/main.css" /> <!-- Permet la liaison du HTML avec le CSS ordinateur -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">